public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Account account1 = new Account("1108", "Quan");
        Account account2 = new Account("0306", "Boi", 1000);
        System.out.println(account1 + ", " + account2);

        account1.credit(2000);
        account2.credit(3000);
        System.out.println(account1 + ", " + account2);

        account1.debit(1000);
        account2.debit(5000);
        System.out.println(account1 + ", " + account2);

        account1.transferTo(account2, 2000);
        System.out.println(account1 + ", " + account2);

        account2.transferTo(account1, 2000);
        System.out.println(account1 + ", " + account2);
    }
}
