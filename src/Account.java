public class Account {
    String id;
    String name;
    int balance = 0;

    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public Account(String id, String name, int balance) {
        this(id, name);
        this.balance = balance;
    }
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public int getBalance() {
        return balance;
    }
    public int credit(int amount) {
        this.balance = this.balance + amount;
        return balance;
    }
    public int debit(int amount) {
        if (amount <= this.balance) {
            this.balance = this.balance - amount;
        }
        else {
            System.out.println("Amount exceeded balance");
        }
        return this.balance;
    }
    public int transferTo(Account another, int amount) {
        if (amount <= this.balance) {
            this.balance = this.balance - amount;
            another.balance = another.balance + amount;
        }
        else {
            System.out.println("Amount exceeded balance");
        }
        return this.balance;
    }

    @Override
    public String toString() {
        return "Account[id=" + getId() + ",name=" + getName() + ",balance=" + getBalance() + "]";
    }
}
